const path = require('path');
const webpack = require('webpack');

module.exports = {
    mode: "development",
    watch: true,
    entry: './assets/react/main.jsx',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'public/build/')
    },
    module: {
            rules: [
                { 
                    test: /\.jsx$/, 
                    exclude: /node_modules/,
                    use: [{
                      loader: 'babel-loader',
                      options: {
                          cacheDirectory: true,
                          babelrc: false,//true para leerlos aqui se pone flase y se descomena,
                          presets: [
                              [
                                  "@babel/env", 
                                  {
                                      "targets": {
                                          'browsers': ['Chrome >=59']
                                      },
                                      "modules":false,
                                      "loose":true
                                  }
                              ],  
                              "@babel/preset-react",
                              
                          ],
                      }
                  }
                  ]
                    
                  },
                {
                    test: /\.css$/,
                    use: [
                    'style-loader',
                    'css-loader'
                    ]
                },
                {
                    test: /\.scss$/,
                    use: [
                            { loader: "style-loader" }, 
                            { loader: "css-loader" }, 
                            {
                                loader: "sass-loader",
                            }
                        ]
                },
            ]
    }
};
