<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    

    /**
     * @Route("/{_locale}/logout",
     *     requirements={"_locale"="en|es|fr|it|pt"},
     *     name="logout")
     */
    public function logout()
    {

    }

    /**
     * @Route("/{_locale}/login",
     *     requirements={"_locale"="en|es|fr|it|pt"},
     *     name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        if($this->getUser()){
            return $this->redirectToRoute('index',[],301);
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('front/_singin.html.twig', 
            [
                'last_username' => $lastUsername, 
                'error' => $error
                ]
            );
    }

}
