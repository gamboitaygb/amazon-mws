<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Utils;
use MCS;
//use ygb\MarketplaceWebService\MarketplaceWebService_Client;

class FrontController extends AbstractController
{
    /**
     * @Route("/", name="init")
     */
    public function init(Request $request)
    {
        return $this->redirectToRoute('index',[],301);
    }
    /**
     * @Route("/{_locale}/",
     *     requirements={"_locale"="en|es|fr|it|pt"},
     *     name="index")
     */
    public function index()
    {
        

         if($this->getUser()){
            return $this->render('front/index.html.twig', 
            [
                'data'=>'Yus'
                ]
            );
         }

         return $this->render('front/index.html.twig', [
            'error' => '',
            'last_username',''
        ]);
    }

    /**
     * @Route("/{_locale}/loadproduct/{merchan}/",
     *     requirements={
     *          "_locale"="en|es|fr|it|pt"
     *          },
     *     name="product_greenice")
     */
    public function loadGreeniceProduct(Request $request,$merchan) 
    {

        $redis=new Utils\Redis();
        $robj=$redis->redis(4);
        $arg =$robj->keys($merchan);



        if(!is_array($arg) || count($arg)==0){
            throw new \Exception('SORRY: This channel do not exist!!');
        }else if(!$this->getUser()){
            throw new \Exception('SORRY: you are not authorize!!');
        }

        include_once($this->getParameter('kernel.project_dir').'/vendor/ygb/amazonmws/src/MarketplaceWebService/Client.php');

        $client = new \MarketplaceWebService_Client(
            'AKIAJG6PKKR2XYXVBOBQ',
            'wpXWp9iA3iSLLaZ6tdVD/gNvng5x2VaJX6OpucQM',
            [],
            'Greenice MWS',
            1);

            //creo el feed
            $feed = <<<EOD
<?xml version="1.0" encoding="UTF-8"?>
<AmazonEnvelope xsi:noNamespaceSchemaLocation="amzn-envelope.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>M_MWSTEST_49045593</MerchantIdentifier>
    </Header>
    <MessageType>Product</MessageType>
    <Message>
        <MessageID>1</MessageID>
        <OperationType>Update</OperationType>
        <Product>
            <SKU>RO7WA11930KB1CASA</SKU>
            <StandardProductID>
                <Type>UPC</Type>
                <Value>4015643103921</Value>
            </StandardProductID>
            <ProductTaxCode>A_GEN_NOTAX</ProductTaxCode>
            <DescriptionData>
                <Title>Titulo del producto</Title>
                <Brand>Greenice</Brand>
                <Description>Una descripción corta del producto.</Description>
                <BulletPoint>Example Bullet Point 1</BulletPoint>
                <BulletPoint>Example Bullet Point 2</BulletPoint>
                <MSRP currency="EUR">25.19</MSRP>
                <Manufacturer>Manufacturer</Manufacturer>
                <ItemType>example-item-type</ItemType>
            </DescriptionData>
            <ProductData>
                    <ProductType>
                        <VariationData>
                            <Parentage>child</Parentage>
                            <VariationTheme>Color</VariationTheme>
                            <Color>red</Color>
                        </VariationData>
                    </ProductType>
            </ProductData>
        </Product>
    </Message>
</AmazonEnvelope>
EOD;

            $feedHandle = @fopen('php://memory', 'rw+');
            fwrite($feedHandle, $feed);
            rewind($feedHandle);
            include_once($this->getParameter('kernel.project_dir').'/vendor/ygb/amazonmws/src/MarketplaceWebService/Model/SubmitFeedRequest.php');
            $request = new \MarketplaceWebService_Model_SubmitFeedRequest();
            $request->setMerchant('A20NHHTW9IQVD4');
            $request->setMarketplaceIdList(array('Id'=>'A1RKKUPIHCS9HS'));
            $request->setFeedType('_POST_PRODUCT_DATA_');
            $request->setContentMd5(base64_encode(md5(stream_get_contents($feedHandle), true)));
            rewind($feedHandle);
            $request->setPurgeAndReplace(false);
            $request->setFeedContent($feedHandle);
            //$request->setMWSAuthToken('<MWS Auth Token>'); // Optional

            rewind($feedHandle);

            $this->invokeSubmitFeed($client, $request);
            

       /* $client = new MCS\MWSClient([
            'Marketplace_Id' => 'A1RKKUPIHCS9HS',
            'Seller_Id' => 'A20NHHTW9IQVD4',
            'Access_Key_ID' => 'AKIAJG6PKKR2XYXVBOBQ',
            'Secret_Access_Key' => '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',
            'MWSAuthToken' => '' // Optional. Only use this key if you are a third party user/developer
        ]);

        if (!$client->validateCredentials()) {
            throw new \Exception('OPPS: Something is not working !!');
        }
        $result = $client->ListMatchingProducts("Greenice");

        print_r($result);exit;

        $em = $this->getDoctrine()->getManager();
        $RAW_QUERY = 'SELECT id_product,id_category_default,ROUND(price,2),date_upd FROM ps_product_shop where 1';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();

        $result = $statement->fetchAll();*/

        //return $this->json(['products' => $result]);

        return $this->render('front/_products.html.twig', 
            [
                'products' => '',//$result, 
                ]
            );

    }

    public function invokeSubmitFeed($service, $request) 
    {
        try {
                $response = $service->submitFeed($request);
                
                  echo ("Service Response\n");
                  echo ("=============================================================================\n");
  
                  echo("        SubmitFeedResponse\n");
                  if ($response->isSetSubmitFeedResult()) { 
                      echo("            SubmitFeedResult\n");
                      $submitFeedResult = $response->getSubmitFeedResult();
                      if ($submitFeedResult->isSetFeedSubmissionInfo()) { 
                          echo("                FeedSubmissionInfo\n");
                          $feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
                          if ($feedSubmissionInfo->isSetFeedSubmissionId()) 
                          {
                              echo("                    FeedSubmissionId\n");
                              echo("                        " . $feedSubmissionInfo->getFeedSubmissionId() . "\n");
                          }
                          if ($feedSubmissionInfo->isSetFeedType()) 
                          {
                              echo("                    FeedType\n");
                              echo("                        " . $feedSubmissionInfo->getFeedType() . "\n");
                          }
                          if ($feedSubmissionInfo->isSetSubmittedDate()) 
                          {
                              echo("                    SubmittedDate\n");
                              echo("                        " . $feedSubmissionInfo->getSubmittedDate()->format('Y-m-d\TH:i:s.\\0\\0\\0\\Z') . "\n");
                          }
                          if ($feedSubmissionInfo->isSetFeedProcessingStatus()) 
                          {
                              echo("                    FeedProcessingStatus\n");
                              echo("                        " . $feedSubmissionInfo->getFeedProcessingStatus() . "\n");
                          }
                          if ($feedSubmissionInfo->isSetStartedProcessingDate()) 
                          {
                              echo("                    StartedProcessingDate\n");
                              echo("                        " . $feedSubmissionInfo->getStartedProcessingDate()->format('Y-m-d\TH:i:s.\\0\\0\\0\\Z') . "\n");
                          }
                          if ($feedSubmissionInfo->isSetCompletedProcessingDate()) 
                          {
                              echo("                    CompletedProcessingDate\n");
                              echo("                        " . $feedSubmissionInfo->getCompletedProcessingDate()->format('Y-m-d\TH:i:s.\\0\\0\\0\\Z') . "\n");
                          }
                      } 
                  } 
                  if ($response->isSetResponseMetadata()) { 
                      echo("            ResponseMetadata\n");
                      $responseMetadata = $response->getResponseMetadata();
                      if ($responseMetadata->isSetRequestId()) 
                      {
                          echo("                RequestId\n");
                          echo("                    " . $responseMetadata->getRequestId() . "\n");
                      }
                  } 
  
                  echo("            ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
       } catch (MarketplaceWebService_Exception $ex) {
           echo("Caught Exception: " . $ex->getMessage() . "\n");
           echo("Response Status Code: " . $ex->getStatusCode() . "\n");
           echo("Error Code: " . $ex->getErrorCode() . "\n");
           echo("Error Type: " . $ex->getErrorType() . "\n");
           echo("Request ID: " . $ex->getRequestId() . "\n");
           echo("XML: " . $ex->getXML() . "\n");
           echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
       }
   }

    /**
     * @Route("/{_locale}/loadchannels/",
     *     requirements={
     *          "_locale"="en|es|fr|it|pt"
     *          },
     *     name="load_channels")
     */ 
    public function loadchannels()
    {
        $redis=new Utils\Redis();
        $robj=$redis->redis(4);
        $arg =$robj->keys('*');

        $tem=$json=array();

        foreach ($arg as $value){
            $i = $robj->hgetall($value);
            $json[]= $i;
        }
        $json_data = json_encode(['channels'=>$json]);
        file_put_contents($this->getParameter('kernel.project_dir').'/assets/js/react/Components/channel.json',$json_data);
    }

    /**
     * @Route("/{_locale}/channel/create/",
     *     requirements={
     *          "_locale"="en|es|fr|it|pt"
     *          },
     *     name="create")
     */
    public function create(Request $request)
    {
            if ($_FILES['image']['name']!="" && $_POST['name']!='' && $_POST['description']!='') {
                $file = $_FILES['image']['name'];
                $target_dir = $this->getParameter('kernel.project_dir') . '/public/img/';
                $path = pathinfo($file);
                $filename = time().'_'.$path['filename'];
                $ext = $path['extension'];
 
                if ($ext!='jpg' && $ext!='png' && $ext!='jpeg') {
                    echo json_encode(['success'=>0,'ext'=>false]);
                    exit;
                } else {
                    $temp_name = $_FILES['image']['tmp_name'];
                    $path_filename_ext = $target_dir.$filename.".".$ext;
 

                    if (!move_uploaded_file($temp_name, $path_filename_ext)) {
                        echo json_encode(['success'=>0,'error'=>false]);
                        exit;
                    } else {
                        
                        //redis para crear el canal, o si se usara base de datos aqui iria el tema
                        $name=$_POST['name'];
                        $desc = $_POST['description'];
                        $file = $filename.".".$ext;
                        $redis=new Utils\Redis();
                        $robj=$redis->redis(4);
                        $robj->hmset($name,['imagepath'=>$file,'channeldescription'=>$desc,'channelname'=>$name]);
                        //$robj->expire($token,86400);
                        $arg =$robj->keys('*');

                        $tem=$json=array();
                
                        foreach ($arg as $value){
                            $i = $robj->hgetall($value);
                            $json[]= $i;
                        }
                        $json_data = json_encode(['channels'=>$json]);
                        file_put_contents($this->getParameter('kernel.project_dir').'/assets/react/Components/channel.json',$json_data);
                   
                         $robj->disconnect();

                        echo json_encode(['success'=>1]);
                        exit;
                    }
                }
            }
        

    }
}
