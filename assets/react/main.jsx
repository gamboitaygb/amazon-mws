import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/index.css';
import Channels from './Channels.jsx';

ReactDOM.render(<Channels />, document.getElementById('channels'));