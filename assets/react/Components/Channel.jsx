import React, {Component} from 'react';
import axios from 'axios';

class Channel extends Component{
    constructor(){
        super();
        this.state={
            channelname:'',
            channeldescription:'',
            imgchannel:'',
        }
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInput( e )
    {
        const {value,name} = e.target;
        this.setState({
            [name]:value
        });
    }

    handleFileUpload( e )
    {
        this.setState({
            imgchannel:e.target.files[0],
        });
    }

    handleSubmit( e )
    {
        e.preventDefault();
            const fd = new FormData();
            fd.append('image',this.state.imgchannel,this.state.imgchannel.name);
            fd.append('name',this.state.channelname);
            fd.append('description',this.state.channeldescription);
            axios.post('https://mktpamazon.com.io/en/channel/create/',fd,{
                onUploadProgress:progressEvent => {
                    console.log('Upload progress' + Math.round(progressEvent.loaded / progressEvent.total * 100) + '%');
                }
            })
            .then(response=> {
                if(response.status==200){

                }
            });



            this.props.onAddChannel(this.state);
    }

    render(){
        return(
            <div className="col-4 float-left mt-4">
                <div className="card">
                    <div className="card-body">
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <input type="text" 
                                className="form-control" 
                                placeholder="Nombre"
                                id="channelname"
                                onChange={this.handleInput}
                                name="channelname"/>
                            </div>
                            <div className="form-group">
                                <input type="text" 
                                className="form-control" 
                                placeholder="Description"
                                id="channeldescription"
                                onChange={this.handleInput}
                                name="channeldescription"/>
                            </div>
                            <div className="form-group">
                            <input style={{display:'none'}} type="file" 
                                className="form-control"
                                onChange={this.handleFileUpload}
                                id="imgchannel"
                                name="imgchannel"
                                ref={fileInput => this.fileInput = fileInput }
                                />
                              <button className="btn-channel" onClick={() => this.fileInput.click()} >Subir Imagen</button>  
                            </div>    
                            <div className="form-group">    
                                <button type="submit" className="btn-channel">Crear</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Channel;