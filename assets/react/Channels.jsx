import React, { Component } from 'react';
import ch from './Components/channel.json';
import axios from 'axios';
import Channel from './Components/Channel.jsx';

class Channels extends Component {
    constructor(props)
    {
        super(props);
        /*this.state={
            url:'/'+document.documentElement.lang+'/loadproduct/greenice',
        };*/
        this.state={
            channels:ch.channels
          }
          this.handleAddChannel=this.handleAddChannel.bind(this);  
    }

    handleAddChannel(channel){
        this.setState({
            channels:[...this.state.channels,channel]
        })
      }

      removeChannel(i){
        if(window.confirm('Seguro que quieres eliminarlo?')){
          this.setState({
            channels:this.state.channels.filter((value,index)=>{
                return index!==i;
            })
          })
        }
      }

    render()
    {
        const channel = this.state.channels.map((channel,index)=>{
            return(
                <div className="card col-12 col-sm-6 col-md-3 mt-4">
                    <div className="card-img">
                        <img className="logo" src={"/img/"+channel.imagepath} alt={channel.channelname}></img>
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">{channel.channelname}</h5>
                        <p className="card-text">{channel.channeldescription}</p>
                        
                    </div>
                    <hr/>
                    <div className="card-footer">
                        <a href={'/'+document.documentElement.lang+'/loadproduct/'+channel.channelname} className="btn-channel">Ver canal</a>
                        <a href="#" 
                        className="btn-channel delete"
                        onClick={this.removeChannel.bind(this,index)}
                        >Eliminar
                        </a>
                    </div>
                </div>

            )
          })
        return(
            /*<div className="card col-12 col-sm-6 col-md-3 mt-5">
                <div className="card-img">
                    <img className="logo" src="/img/amazon.png" alt="Amazon"></img>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Amazon</h5>
                    <p className="card-text">Marketplace</p>
                    <a href={this.state.url} className="btn btn-primary">Ver canal</a>
                </div>
            </div>   */
            <div className="row">
                <Channel onAddChannel={this.handleAddChannel}/> 
                {channel}
            </div>
        )
    }
}
export default Channels;